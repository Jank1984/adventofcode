from functools import reduce
inputs = []
with open('day_1_1_input.txt') as input_f:
    for line in input_f:
        inputs.append(int(line.rstrip()))

print("inputs[:10]", inputs[:10])
print("len(inputs)", len(inputs))


print("Answer day 1 1:", reduce(lambda a,b: a+b, inputs, 0))