inputs = []
with open('day_1_1_input.txt') as input_f:
    for line in input_f:
        inputs.append(int(line.rstrip()))

# inputs = [3,3,4,-2,-4]#dev
# print(inputs[:10])
sum = 0
sums = set()
for val in inputs:
    sum += val
    sums.add(sum)

idx = 0
loop = 1
while idx < len(inputs):
    sum += inputs[idx]
    if(sum in sums):
        print("Answer day 1 2:", "{sum} found twice. (loop #{loop})".format(sum=sum, loop=loop))
        break
    if((idx+1)%len(inputs) == 0): loop += 1
    idx = (idx+1)%len(inputs)


