import random
import math
import re
import argparse
import string
parser = argparse.ArgumentParser()
parser.add_argument("-c", "--colors", help="Farbige ausgabe der visuellen Ausgabe", action="store_true")
parser.add_argument("-v", "--visual", help="Visuelle Ausgabe der patches", action="store_true")
parser.add_argument("-a", "--visual_all", help="Visuelle des gesamten", action="store_true")
parser.add_argument("-m", "--showmatrix", help="show intersect matrix", action="store_true")
parser.add_argument("-z", "--zoomfactor", help="zoomfactor bei visual_all", type=float)
parser.add_argument("-f", "--file", help="andere datei als day_3_1_input.txt")
args = parser.parse_args()


coloredTerm = args.colors or False
visual = args.visual or False
visual_all = args.visual_all or False
zoomfactor_args = args.zoomfactor

colors = {}
# colors["black"] ='\033[30m' if coloredTerm else ""
colors["red"] ='\033[31m' if coloredTerm else ""
colors["green"] ='\033[32m' if coloredTerm else ""
colors["orange"] ='\033[33m' if coloredTerm else ""
colors["blue"] ='\033[34m' if coloredTerm else ""
colors["purple"] ='\033[35m' if coloredTerm else ""
colors["cyan"] ='\033[36m' if coloredTerm else ""
colors["lightgrey"] ='\033[37m' if coloredTerm else ""
colors["darkgrey"] ='\033[90m' if coloredTerm else ""
colors["lightred"] ='\033[91m' if coloredTerm else ""
colors["lightgreen"] ='\033[92m' if coloredTerm else ""
colors["yellow"] ='\033[93m' if coloredTerm else ""
colors["lightblue"] ='\033[94m' if coloredTerm else ""
colors["pink"] ='\033[95m' if coloredTerm else ""
colors["lightcyan"] ='\033[96m' if coloredTerm else ""
colors["reset"] ='\033[0m' if coloredTerm else ""
def randColor(): return random.choice(list(colors.values()))
def hashedColor(n): return list(colors.values())[n%len(colors)]


pattern = r"#(?P<id>\d+)\s*.\s*(?P<x>\d+),(?P<y>\d+):\s*(?P<width>\d+)x(?P<height>\d+)"
inputs = []
clams = {}
# with open('day_3_1_input_test.txt') as input_f: #dev test file
with open(args.file or 'day_3_1_input.txt') as input_f: #dev test file
    for line in input_f:#
        line_stripped = line.rstrip()
        inputs.append(line_stripped)
        match =  re.search(pattern, line_stripped)
        if match:
            clams[match.group("id")] = \
            {\
                "x": int(match.group("x")),\
                "y": int(match.group("y")),\
                "width": int(match.group("width")),\
                "height": int(match.group("height")),\
                "w": int(match.group("x"))+int(match.group("width")),\
                "h": int(match.group("y"))+int(match.group("height"))\
                }

def get_collision(x, y, w, h, x2, y2, w2, h2):
    return (((x < x2 < w) or (x < w2 < w)) and ((y < y2 < h) or (y < h2 < h)))


def intersection(x, y, w, h, x2, y2, w2, h2):
    intersect_x = max(x, x2)
    intersect_y = max(y, y2)
    intersect_w = min(w, w2)
    intersect_h = min(h, h2)
    return (intersect_x, intersect_y, intersect_w, intersect_h)

def intersection_delta(x, y, w, h, x2, y2, w2, h2):
    _x, _y, _w, _h = intersection(x, y, w, h, x2, y2, w2, h2)
    dw = abs(_w-_x)
    dh = abs(_h-_y)
    return (dw, dh)

def visualise_intersection(x, y, w, h, x2, y2, w2, h2):
    canvas_x_min = int(min(x,x2))
    canvas_x_max = int(max(w, w2))
    canvas_y_min = int(min(y,y2))
    canvas_y_max = int(max(h, h2))
    for height in range(canvas_y_min, canvas_y_max):
        for width in range(canvas_x_min, canvas_x_max):
            if x < width <= w and y < height <= h:
                if x2 < width <= w2 and y2 < height <= h2:
                    print(colors["red"]+'X'+ colors["reset"], end='')
                else:
                    print(colors["blue"]+'A'+ colors["reset"], end='')
            elif x2 < width < w2 and y2 < height <= h2:
                print(colors["green"]+'B'+ colors["reset"], end='')
            else: print('.', end='')
        print('\n', end='')
    print('\n', end='')

def visualise_claims(_clams):
    alphabet = list(string.ascii_uppercase+string.digits)


    

    canvas_x_min = 0
    canvas_x_max = 0
    canvas_y_min = 0
    canvas_y_max = 0
    for key in _clams:
        c = _clams[key]
        canvas_x_max = max(int(c["w"])+1 , canvas_x_max)
        canvas_y_max = max(int(c["h"])+1 , canvas_y_max)
    
    factor = zoomfactor_args # zoom factor
    if not zoomfactor_args:
        factor = max(canvas_x_max,canvas_y_max)/60
        factor = max(factor, 1)


    for height in range(canvas_y_min,  math.floor(canvas_y_max/factor)):
        for width in range(canvas_x_min, math.floor(canvas_x_max/factor)):
                char_to_print = "."
                for key in _clams:
                    num = int(key)%len(alphabet)
                    letter = alphabet[int(key)%len(alphabet)]
                    c = _clams[key]
                    if int(c["x"]) < math.floor(width*factor) <= int(c["w"]) and int(c["y"]) < math.floor(height*factor) <= int(c["h"]):
                        char_to_print = hashedColor(num)+letter+colors["reset"]
                        break
                print(char_to_print, end='')
        print('\n', end='')



dim_x = 1
dim_y = 1
for key in clams:
    c1 = clams[key]
    dim_x = max(c1["w"] , dim_x)
    dim_y = max(c1["h"] , dim_y)

intersection_matrix = [[0 for x in range(dim_y)] for y in range(dim_x)] 
# print(intersection_matrix)

# sum_sq_inches = 0
# intersect_pairs = []
for key in clams:
    c1 = clams[key]
    for key2 in clams:
        c2 = clams[key2]
        if key==key2: continue
        # if (int(key2), int(key)) in intersect_pairs: continue
        if get_collision(c1["x"], c1["y"], c1["w"], c1["h"], c2["x"], c2["y"], c2["w"], c2["h"]):
            w, h = intersection_delta(c1["x"], c1["y"], c1["w"], c1["h"], c2["x"], c2["y"], c2["w"], c2["h"])
            x, y, w, h = intersection(c1["x"], c1["y"], c1["w"], c1["h"], c2["x"], c2["y"], c2["w"], c2["h"])
            for idx_x in range(x, w):
                for idx_y in range(y, h):
                    intersection_matrix[idx_y][idx_x] = 1
            # intersect_pairs.append((int(key), int(key2)))
            if visual: visualise_intersection(c1["x"], c1["y"], c1["w"], c1["h"], c2["x"], c2["y"], c2["w"], c2["h"])
            # sum_sq_inches += w*h

if visual_all: visualise_claims(clams)

if args.showmatrix:
    for l in intersection_matrix:
        for i in l:
            print(i, end='')
        print('\n', end='')

print ("Answer day 3 1:\n", sum([sum(inner) for inner in intersection_matrix]))
# print ("Answer day 3 1:\n", sum_sq_inches)