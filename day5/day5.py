
# coding: utf-8

# In[1]:
# import re
# from datetime import datetime
import string
# string.ascii_letters 
lowercase = list(string.ascii_lowercase)
uppercase = list(string.ascii_uppercase)
# In[2]:

def react_repl(_chain):
    chain = _chain
    reactionHappend = True
    while reactionHappend:
        reactionHappend = False
        len_of_chain = len(chain)
        for idx, char in enumerate(lowercase):
            chain = chain.replace(lowercase[idx]+uppercase[idx], "")
            chain = chain.replace(uppercase[idx]+lowercase[idx], "")
            if len(chain) != len_of_chain: reactionHappend = True
        # print("len_of_chain", len_of_chain)
    return len_of_chain 


def react(_chain):
    # if not isinstance(_chain, (list,)):
    #     _chain = list(_chain)    
    removal_list = []
    reactionHappend = True
    while reactionHappend:
        reactionHappend = False
        idx = 0
        while idx < len(_chain):
            if _chain[idx] in lowercase:
                if idx+1 < len(_chain):
                    if _chain[idx+1] in uppercase:
                        a_alpha_index = lowercase.index(_chain[idx])
                        b_alpha_index = uppercase.index(_chain[idx+1])
                        if a_alpha_index == b_alpha_index:
                            removal_list.insert(0, idx)
                            reactionHappend = True
                            idx +=2 # skip found partner

            if _chain[idx] in uppercase:
                if idx+1 < len(_chain):
                    if _chain[idx+1] in lowercase:
                        a_alpha_index = uppercase.index(_chain[idx])
                        b_alpha_index = lowercase.index(_chain[idx+1])
                        if a_alpha_index == b_alpha_index:
                            removal_list.insert(0, idx)
                            reactionHappend = True
                            idx +=2 # skip found partner
            idx += 1

        # print("rm #removal_list elem", len(removal_list))
        for idx_del in removal_list:
            del _chain[idx_del:idx_del+2]
        removal_list = []
        # print("len(_chain)", len(_chain))
        # if len(_chain) < 400: print("_chain", _chain)
    

    print("final\tlen(_chain)", len(_chain))
    return len(_chain)

# In[3]:   
# with open('day_5_input_test.txt') as input_f:
with open('day_5_input.txt') as input_f:

    chain = None
    for line in input_f:
        line_stripped = line.rstrip()
        chain = line_stripped
    # chainList = list(chain)

    # print("Answer day 5 1:", react(list(chain)))
    print("Answer day 5 1:", react_repl(chain))

    min_react = None
    for idx, char in enumerate(lowercase):
        chain_without_char = chain.replace(lowercase[idx], "").replace(uppercase[idx], "")
        react_chain = react_repl(chain_without_char)
        if not min_react: min_react = react_chain
        min_react = min(min_react, react_chain)
        print("rm", lowercase[idx]+"/"+uppercase[idx], react_chain)

    print("Answer day 5 2:", min_react)



