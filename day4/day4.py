
# coding: utf-8

# In[129]:


import re
import datetime


# In[130]:


pattern = r"^\[(?P<date>.+)\](?P<event>(?P<sleep>\sfalls\sasleep)|(?P<wake>\swakes\sup)|(?P<shift>\sGuard\s#(?P<guardNum>\d+)\sbegins\sshift))"
events = []
#events_by_date = {}

with open('day_4_input.txt') as input_f: #dev test file
    for line in input_f:
        line_stripped = line.rstrip()
        match =  re.search(pattern, line_stripped)
        if match:    
            event = None
            guardNum = None
            if match.group("shift"): 
                event = "shift"
                guardNum = match.group("guardNum")
            if match.group("sleep"): event = "sleep"
            if match.group("wake"): event = "wake"
             
            date_time_obj = datetime.datetime.strptime(match.group("date"), '%Y-%m-%d %H:%M')
                        
            events.append({
                "date": date_time_obj,
                "event": event,
                "guardNum": guardNum
            })
            
            
events_sorted =  sorted(events, key=lambda p: p["date"], reverse=False)
                
                      
                      


# In[131]:

if False:
    for e in events[:5]:
        print(e['date'].date(), end=',')
        print(e['date'].time(), end=', ')
        print(e['event'], end=',\t')
        print(e['guardNum'], end='\n')


# In[132]:

if False:
    for e in events_sorted[:5]:
        print(e['date'].date(), end=',')
        print(e['date'].time(), end=', ')
        print(e['event'], end=',\t')
        print(e['guardNum'], end='\n')


# In[133]:


events_by_day = {}
current_guard = None
for e in events_sorted:   
    if e['event'] == "shift":
        current_guard = e['guardNum']
        continue
    date = str(e['date'].date()).replace("-","_")
    if not date in events_by_day:
        events_by_day[date] = []
        
    events_by_day[date].append({"min": e["date"].time(),\
                                                        "sleep": e["event"] == "sleep",\
                                                        "guard": current_guard\
                                                        })


# In[134]:

if False:
    for key in events_by_day:
        print("day {day}".format(day=key))
        for e in events_by_day[key]:
            print(e)
    

# In[135]:

if False:
    print("Date\tId\tMinute")
    table_margin_left = "\t\t"
    print(table_margin_left, end='')
    for i in range(0, 6): 
        for j in range(0, 10): 
            print( i,end='')
    print("", end='\n')
    print(table_margin_left, end='')
    for i in range(0, 6): 
        for j in range(0, 10): 
            print( j,end='') 
    print("", end='\n')
            
    for day in events_by_day:
        print(day[5:], end='\t')
        print(events_by_day[day][0]["guard"], end='\t')
        wake_status = True
        for minutes in range(0, 60):
            for event_min in events_by_day[day]:
                if minutes == event_min["min"].minute:
                    wake_status = not event_min["sleep"]
                    break
            print("." if wake_status else "#" , end='')   
        print("", end='\n')


# In[139]:


events_by_guard = {}

for day in events_by_day:
    eday = events_by_day[day]
    for e in eday:
        guard = e["guard"]
        if not guard in events_by_guard: events_by_guard[guard] = []
        obj = {\
                "min": e["min"].minute,\
                "sleep": e["sleep"],\
                "day": day[5:]
                }
        events_by_guard[guard].append(obj)
        


# In[140]:


# sleep_matrix = {}
# for guard in events_by_guard:
#     event_list = events_by_guard[guard]
#     #print(event_list)
#     if not guard in sleep_matrix: sleep_matrix[guard] = []
#     wake_status = 0
#     for minutes in range(0, 60):
#         for e in event_list:
#             if minutes == e["min"]:
#                 wake_status = not e["sleep"]
#         sleep_matrix[guard].append(wake_status)

# sleep_matrix


# In[141]:


for guard in events_by_guard:
     event_list = events_by_guard[guard]
     #print(event_list)


# In[142]:
events_by_guard_by_day = {}
for guard in events_by_guard.keys(): 
    events_by_guard_by_day[guard] = {}
    for e in events_by_guard[guard]:
        day = e["day"]
        if not day in events_by_guard_by_day[guard]:
            events_by_guard_by_day[guard][day] = []
        events_by_guard_by_day[guard][day].append({"min": e["min"], "sleep": e["sleep"]})

#events_by_guard_by_day

# In[142]:

events_by_guard_by_day_matrix = {}
for guard in events_by_guard_by_day:
    day_list = events_by_guard_by_day[guard]
    if not guard in events_by_guard_by_day_matrix:
        events_by_guard_by_day_matrix[guard] = {}
    for day in day_list:
        events = day_list[day]
        if not day in events_by_guard_by_day_matrix[guard]:
            events_by_guard_by_day_matrix[guard][day] = []

        wake_status = 0
        for minutes in range(0, 60):

            for e in events:
                if e["min"] == minutes:
                    wake_status = 1 if e["sleep"] else 0

            events_by_guard_by_day_matrix[guard][day].append(wake_status)
            



#events_by_guard_by_day_matrix

# In[143]:

minutes_sleep_by_guard = {}
minute_sum_sleep_by_guard = {}
for guard in events_by_guard_by_day_matrix:
    day_list = events_by_guard_by_day_matrix[guard]
    minutes_sleep_by_guard[guard] = sum([sum(day_list[day]) for day in day_list])

    sum_sleep_min = []
    for minutes in range(0, 60):
        sum_sleep_min.append(sum([day_list[day][minutes] for day in day_list]))
        


    minute_sum_sleep_by_guard[guard] = sum_sleep_min

#minute_sum_sleep_by_guard

# In[144]:

print("minutes_sleep_by_guard:")
index = list(minutes_sleep_by_guard.values()).index(max(list(minutes_sleep_by_guard.values())))
max_minute_guard = list(minutes_sleep_by_guard.keys())[index]
max_minute_min = max(list(minutes_sleep_by_guard.values()))
print("guard", end='\t')
print(max_minute_guard, end="\t")
# print(minutes_sleep_by_guard.index(max(minutes_sleep_by_guard)))
print("minutes", end='\t')
print(max_minute_min)

    
# In[145]:
# print("max_minute_sum_sleep_by_guard:")
# # max_min = 
# for guard in minute_sum_sleep_by_guard:
#     minutes_list = minute_sum_sleep_by_guard[guard]
#     print(guard, end='\t')
#     print(minutes_list.index(max(minutes_list)))

# # max_minute_guard

# In[146]:
print("max minute of max_minute_guard")
max_minute_of_max_guard = minute_sum_sleep_by_guard[max_minute_guard].index(max(minute_sum_sleep_by_guard[max_minute_guard]))
print(minute_sum_sleep_by_guard[max_minute_guard].index(max(minute_sum_sleep_by_guard[max_minute_guard])))
# In[147]:
print("Day 4_1:"," guard x max minute")
print("{guard}x{max_minute}={mul}".format(\
                                        guard= max_minute_guard,\
                                        max_minute= max_minute_of_max_guard,\
                                        mul=int(max_minute_guard)*max_minute_of_max_guard))


# In[147]:

minutes_most_frequent = [{"guard": None, "sum": 0} for x in range(0,60)]
for minute_idx, minute in enumerate(minutes_most_frequent):
    for guard in minute_sum_sleep_by_guard:
        minute_sums = minute_sum_sleep_by_guard[guard]
        sum_of_current_min = minute_sums[minute_idx]
        if minutes_most_frequent[minute_idx]["sum"] < sum_of_current_min:
            # minutes_most_frequent[minute_idx]["sum"] === minute["sum"]
            minutes_most_frequent[minute_idx] = {"guard": guard, "sum": sum_of_current_min}
        
    

max_freq_val = -1
max_freq_guard = -1
max_freq_min = -1
for idx, minute in enumerate(minutes_most_frequent):
    if max_freq_val < minute["sum"]:
        max_freq_val = minute["sum"]
        max_freq_guard = minute["guard"]
        max_freq_min = idx


print("Day 4_2:"," guard max frequent minute sleep", "max_freq_val", max_freq_val, "max_freq_guard", max_freq_guard)



print("max_freq_min:{max_freq_min} x max_freq_guard:{max_freq_guard} = {prod}".format(\
    max_freq_min=max_freq_min,\
    max_freq_guard=max_freq_guard,\
    prod=max_freq_min*int(max_freq_guard)))