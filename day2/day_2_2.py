from functools import reduce
inputs = []
with open('day_2_1_input.txt') as input_f:
    for line in input_f:
        inputs.append(line.rstrip())

# with_two = 0
# with_three = 0
# for pack_id in inputs:
#     has_two = False
#     has_three = False
#     for char in pack_id:
#         frq = pack_id.count(char)
#         if frq == 2 and not has_two: 
#             with_two += 1
#             has_two = True
#         if frq == 3 and not has_three: 
#             with_three += 1
#             has_three = True

def hamming_distance(s1, s2):
    assert len(s1) == len(s2)
    return sum(ch1 != ch2 for ch1, ch2 in zip(s1, s2))

def rm_diff(s1, s2):
    assert len(s1) == len(s2)
    assert (hamming_distance(s1, s2) == 1)
    for ch1, ch2 in zip(s1, s2):
        if ch1 != ch2:
            return s1.replace(ch1, "")

hamming_1_found = False
for word1 in inputs:
    # print(word1)
    for word2 in inputs:
        if hamming_distance(word1, word2) == 1:
            print ("Answer day 2 2:\n","Abstand von 1 bei woertern {word1}, {word2}".format(word1=word1, word2=word2))
            print("without diff", rm_diff(word1, word2))
            hamming_1_found = True
            break
    if hamming_1_found: break



# print ("Answer day 2 2:\n", "2:{two}, 3:{three}, product:{two_mul_three}".format(two=with_two, three=with_three, two_mul_three=with_two*with_three))



