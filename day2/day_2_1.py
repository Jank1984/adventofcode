from functools import reduce
inputs = []
with open('day_2_1_input.txt') as input_f:
    for line in input_f:
        inputs.append(line.rstrip())

with_two = 0
with_three = 0
for pack_id in inputs:
    has_two = False
    has_three = False
    for char in pack_id:
        frq = pack_id.count(char)
        if frq == 2 and not has_two: 
            with_two += 1
            has_two = True
        if frq == 3 and not has_three: 
            with_three += 1
            has_three = True

print ("Answer day 2 1:\n", "2:{two}, 3:{three}, product:{two_mul_three}".format(two=with_two, three=with_three, two_mul_three=with_two*with_three))



