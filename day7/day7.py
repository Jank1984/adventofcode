
# coding: utf-8

# In[1]:
import re
# In[2]:
pattern = r"^Step\s{1}(?P<a>\w{1})\s{1}must be finished before step\s{1}(?P<b>\w{1}) can begin\.$"
edges = []
nodes = set()
with open('day_7_input.txt') as input_f:
# with open('day_7_input_test.txt') as input_f:
    chain = None
    for line in input_f:
        line_stripped = line.rstrip()
        match =  re.search(pattern, line_stripped)
        if match:
            edges.append((match.group("a"), match.group("b")))
            nodes.add(match.group("a"))
            nodes.add(match.group("b"))

# In[3]:
print("edges[:5]", edges[:5])
print("nodes[:5]", [x for x in nodes][:5])
print("nodes len", len([x for x in nodes]))
end = set()
for step in edges: end.add(step[1])
root = sorted(list(nodes.difference(end)))
print("root(s)", root)


# In[4]:
def has_pre_not_visited(_edges, _visited, _x):
    return len(set(pre(_edges, _x)).difference(set(_visited))) > 0

def pre(_edges, _x): return [_elem[0] for _elem in _edges if _elem[1]==_x]

def post(_edges, _x): return [_elem[1] for _elem in _edges if _elem[0]==_x]


def dfs(_root):
    visited = set()
    path = []
    to_visit = _root or []
    while len(to_visit) > 0:
        next_candidates = [x for x in to_visit if not has_pre_not_visited(edges, visited, x)]
        current_node = next_candidates.pop(0)
        to_visit.remove(current_node)
        
        path.extend(current_node)
        visited.add(current_node)
        destinations = [x[1] for x in edges if x[0] == current_node and not x[1] in visited and not x[1] in to_visit]
        to_visit.extend(destinations)
        to_visit = sorted(to_visit)

    return path


# In[5]:
path = dfs(root)
print("Answer day 7 1:\n", "path", "".join(path))

# In[6]:
